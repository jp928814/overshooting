# -*- coding: utf-8 -*-
"""
Created on Wed May 17 12:32:06 2023

saving map integration analysis

@author: jp928814
"""

import os 
cwd = os.getcwd()
import sys
# import sympy as sp
sys.path.append("./aux_codes") # Directory of auxiliary modules

import numpy as np
import sdeint
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import matplotlib.ticker as ticker
from matplotlib.colors import ListedColormap, BoundaryNorm, LinearSegmentedColormap
from matplotlib import cm
from matplotlib import colormaps as colm
import json
plt.rcParams.update({'font.size': 18})
cmap =colm.get_cmap('turbo')
import datetime
import csv
import pandas as pd
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy.stats as st
plt.close('all')
# plt.ion()
import cmocean as co
import stiched_colormap as scol
from scientific_colourmaps import load_cmap

import tkinter as tk
from tkinter import messagebox, Listbox, END
#%%
'''pop-up folder selection'''

def save_selected_folder():
    selected_folder = listbox.get(tk.ACTIVE)
    root.selection = selected_folder  # Save the selected folder in a new variable
    root.destroy()  # Close the UI
    temp_vars = {'Last_folder': selected_folder}
    with open('temp_analysis.json', 'w') as file: json.dump(temp_vars, file) #update json temp folder.

def re_run():
    try: 
        with open('temp_analysis.json', 'r') as file: temp_vars = json.load(file) #Read last folder.
    except: print('No folder found for last runnned analysis')
    selected_folder = temp_vars['Last_folder']
    root.selection = selected_folder  # Save the selected folder in a new variable
    root.destroy()  # Close the UI

def populate_listbox(directory):
    subfolders = [folder for folder in os.listdir(directory) if os.path.isdir(os.path.join(directory, folder))]
    output_folders = [folder for folder in subfolders if folder.startswith("output_")]
    for folder in output_folders:
        listbox.insert(tk.END, folder)
        
root = tk.Tk()
root.title("Folder Selection")
root.geometry("400x500")  # Set the dimensions of the main window (width x height)
# Set the directory you want to search for subfolders
directory_path = "."
listbox = tk.Listbox(root, selectmode=tk.SINGLE, width=50, height=20)
listbox.pack()
populate_listbox(directory_path)
# Set the active item using its string value
try: 
    with open('temp_analysis.json', 'r') as file: temp_vars = json.load(file) #Read last folder. 
    item_to_select = temp_vars['Last_folder']
    index = listbox.get(0, tk.END).index(item_to_select)
    listbox.activate(index)
    listbox.see(index)
except: print('No folder found for last runnned analysis')
save_button = tk.Button(root, text="Analyze", command=save_selected_folder)
save_button.pack()
last_button = tk.Button(root, text="Re-run previous", command=re_run)
last_button.pack()
root.mainloop()
selected_folder = root.selection  # Retrieve the selected folder from the root object
print("Selected folder:", selected_folder)
folder_name= selected_folder
with open(os.path.join(folder_name,'temp_run_state.json'), 'r') as file: temp_vars = json.load(file)    # Load the JSON data
# folder_name=temp_vars['folder']

# Extract the date from the filename
file_date = selected_folder.split('_')[1:4]
file_date = "_".join(file_date)
# Convert the date string to a datetime object
file_date = datetime.datetime.strptime(file_date, "%d_%m_%Y").date()
# Define the threshold date
threshold_date = datetime.datetime(2023, 6, 1).date()
if file_date < threshold_date: print('use old code')

#%%
###############################################################################

def tscape_to_lscape(t_scape,tfs,deltas,l0,target_ensemble): 
    lmax=25.01+deltas
    tscp_norm=np.zeros_like(t_scape)
    lscape=np.zeros_like(t_scape)
    for c in range(target_ensemble):
        tscp_norm[:,:,c]=t_scape[:,:,c]/tfs
    # print(tscp_norm)
    for d in range(len(deltas)):
        lmax=25.01+deltas[d]
        lscape[:,d,:]=tscp_norm[:,d,:]*(2*(lmax-l0))+l0
    return lscape


cls=np.load(os.path.join(folder_name,'cls.npy'))
deltas=np.load(os.path.join(folder_name,'deltas.npy'))
t_scape=np.load(os.path.join(folder_name,'t_scape.npy'))
tfs=np.load(os.path.join(folder_name,'tfs.npy'))     

if file_date < threshold_date: #to analyze the runs with the old format
    cases=np.shape(t_scape)[2]
    runs=cases*np.ones_like(tfs)
    max_ensemble=cases
    target_ensemble=cases

else: 
    runs=np.load(os.path.join(folder_name,'runs.npy'))     
    max_ensemble,target_ensemble=temp_vars['ensemble_vars']

s=temp_vars['Noise']
l0,x0=temp_vars['Initial conditions']

means=np.zeros((len(cls),len(deltas)))
counts=np.zeros((len(cls),len(deltas)))
varss=np.zeros((len(cls),len(deltas)))
skews=np.zeros((len(cls),len(deltas)))
vars_tuple=[]

        

def bootstrap_skewness(data, n_iterations=1000,Debug=False):
    n = len(data)
    skewness_values = np.zeros(n_iterations)
    for i in range(n_iterations):
            sample = np.random.choice(data, size=n, replace=True)
            skewness = st.skew(sample)
            skewness_values[i] = skewness
    if Debug==True:print('len_sample:', len(sample)/n)    
    
    bootstrap_skewness_mean = np.mean(skewness_values)
    bootstrap_skewness_std = np.std(skewness_values)
    if Debug==True:print('sk mean:', bootstrap_skewness_mean)    
    return bootstrap_skewness_mean, bootstrap_skewness_std


# mean_skewness, std_skewness = bootstrap_skewness(data, n_iterations=1000)
# print("Mean skewness:", mean_skewness)
# print("Standard deviation of skewness:", std_skewness)

# In this version, we use np.zeros(n_iterations) to create an array of zeros with a length equal to n_iterations. Then, during the loop, we assign the compu
lscape=tscape_to_lscape(t_scape,tfs,deltas,l0,target_ensemble)
lscape[lscape<20.01]=np.nan

for d in range(len(deltas)):
    for j in range(len(cls)):
        vars_tuple.append((cls[j],deltas[d]))
        scaped=t_scape[j,d,:][t_scape[j,d,:]>0]
        means[j,d]=(np.mean(scaped))/(tfs[j,d]-2*deltas[d]/cls[j])
        counts[j,d]=len(scaped)
        varss[j,d]=np.var(lscape[j,d,np.isnan(lscape[j,d])==False])
        if len(scaped)>50:
            skews[j,d],std_skewness =bootstrap_skewness(lscape[j,d,np.isnan(lscape[j,d])==False], n_iterations=300)
skews[counts<50]=np.nan   
means[means<0.05]=np.nan     
results=[]
for j in range(len(counts.flatten())):
    results.append((vars_tuple[j][0],vars_tuple[j][1],counts.flatten()[j],means.flatten()[j],varss.flatten()[j],skews.flatten()[j]))      
        
with open('stats2.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Cl', 'delta', 'count','mean','variance','skewness'])  # Write the header row
    writer.writerows(results)  # Write all the results at once

# Read the CSV file into a pandas DataFrame
df = pd.read_csv('stats2.csv')

# Extract variable values and results from the DataFrame
cls_flat = df['Cl'].values
deltas_flat = df['delta'].values
results = df['count'].values
means = df['mean'].values
varss = df['variance'].values
skewss = df['skewness'].values

# Get the unique values and their corresponding indices
unique_cls, cls_indices = np.unique(cls_flat, return_inverse=True)
unique_deltas, delta_indices = np.unique(deltas_flat, return_inverse=True)

# Reshape results array to match the shape of unique_cls and unique_deltas
results_grid = results.reshape(len(unique_cls), len(unique_deltas))
means_grid = means.reshape(len(unique_cls), len(unique_deltas))
varss_grid = varss.reshape(len(unique_cls), len(unique_deltas))
skewss_grid = skewss.reshape(len(unique_cls), len(unique_deltas))
# varss_grid = runs.reshape(len(unique_cls), len(unique_deltas))

#%%
from palettable.colorbrewer.qualitative import Set2_4
# Get the color palette
palette = np.array(Set2_4.colors,dtype=float) 
  
folder_name = f"Analysis"
subfolder_name = f"zones"
try:  os.makedirs(os.path.join(selected_folder,folder_name,subfolder_name))
except: print('folder exists')

from scipy.interpolate import PchipInterpolator
probs_grid=100*results_grid/runs
probs_grid[np.isnan(probs_grid)]=0

quart=[75.,50.,25.]
c_zones=np.empty((len(deltas),len(quart)+2))
c_zones[:,0] = cls[0]
c_zones[:,-1] = cls[-1]



plot_zones=False

x_interp = np.linspace(min(cls), max(cls), 100)  # Adjust the number of points as needed

surf_interp=np.zeros((len(x_interp),len(deltas)))
for d in range(len(deltas)):
    for q in range(len(quart)):
        spline = PchipInterpolator(cls, probs_grid[:,d])
        # Generate values for interpolation
        # Perform spline interpolation
        y_interp = spline(x_interp)
        surf_interp[:,d]=y_interp
        target_y = quart[q]
        index_closest = np.argmin(np.abs(y_interp - target_y))
        c_zones[d,q+1] = x_interp[index_closest]
        if plot_zones==True:
            fig=plt.figure()
            ax=plt.subplot(111)
            plt.plot(cls,probs_grid[:,d],'o',color='b')
            plt.plot(x_interp,y_interp,'--',color='k')
            for q in range(len(quart)):
                plt.axhline(y=quart[q],color='k')
                # if q==2:mylabel='safe'
                # else: mylabel=None
                qplot=ax.axvspan(c_zones[d,q],c_zones[d,q+1], alpha=0.5,color=palette[q]/255)
            ax.axvspan(c_zones[d,3],c_zones[d,4], alpha=0.5,color=palette[3]/255,label='safe')
            plt.legend()
            ax.set_xlabel('$C_\lambda$')
            ax.set_ylabel('Tipped')
            plt.tight_layout()
            try:  fig.savefig(os.path.join(selected_folder,folder_name,subfolder_name,'zones_%.i'%(d)+'.png'), dpi=200) 
            except: print('could not save zones')
            plt.close(fig)
# #%%
# plt.figure()

# cmap1 = plt.cm.RdYlBu_r
# cmap2 = plt.cm.RdYlGn
# cma3=scol.stiched_colormap(cmap1, cmap2, 50)
# plt.pcolor(deltas,x_interp,surf_interp,cmap=cma3)

#%%


deltas_b=np.load('deltas_bound.npy')     
cls_bound=np.load('cls_bound.npy')     
dd=np.diff(deltas)[0]/2
dc=np.diff(cls)[0]/2


def lscape_relative_to_delta(deltas,lscape):
    lrelat=np.zeros_like(lscape)
    for d in range(len(deltas)):
        lrelat[:,d,:]=lscape[:,d,:]-(25.01+deltas[d])
    return lrelat


# Plotting the pcolor
fsz=(18, 11)
fig, axes = plt.subplots(2, 3, sharey=True,figsize=fsz)
ax=axes[0,0]

cmap1 = plt.cm.RdYlBu_r
cmap2 = plt.cm.RdYlGn
cma3=scol.stiched_colormap(cmap1, cmap2, 50)
# im1=ax.pcolormesh(unique_deltas, unique_cls, probs_grid,cmap=cma3)############################
im1=ax.pcolor(deltas,x_interp,surf_interp,cmap=cma3)

for q in range(1,len(quart)+1):
#     ax.fill_between(deltas, c_zones[:,q], c_zones[:,q+1], alpha=0.4,color=palette[q]/255)
    ax.plot(deltas,c_zones[:,q],'o-',color=palette[q]/255)
# for q in range(len(quart)+1):
#     ax.fill_between(deltas, c_zones[:,q], c_zones[:,q+1], alpha=0.4,color=palette[q]/255)
    # ax.plot(deltas,c_zones[:,q],'o-',color=palette[q]/255)


ax.plot(deltas_b,cls_bound,'--k',lw=2,label='deterministic')
ax.set_xlabel('$\delta$')
ax.set_ylabel('$C_\lambda$')
formatter = ticker.ScalarFormatter(useMathText=True)
formatter.set_scientific(True)
formatter.set_powerlimits((0, 1))
ax.yaxis.set_major_formatter(formatter)
ax.set_xlim([np.min(deltas),np.max(deltas)])
ax.set_ylim([0,np.max(cls)])
divider = make_axes_locatable(ax)
ax.legend()
cax = divider.append_axes('right', size='5%', pad=0.05)
cbar=fig.colorbar(im1, cax=cax, orientation='vertical',label='Tipped percentage')
cbar.set_ticks([0,25,50,75,100])

ax=axes[0,1]
im2=ax.pcolormesh(unique_deltas, unique_cls,  means_grid)
# ax.set_xlabel('$\delta$')
# ax.set_ylabel('$C_\lambda$')
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.05)
cbar=fig.colorbar(im2, cax=cax, orientation='vertical',label='Mean scape time')
# formatter = ticker.ScalarFormatter(useMathText=True)
# formatter.set_scientific(True)
# formatter.set_powerlimits((0, 1))
# cbar.ax.yaxis.set_major_formatter(formatter)


ax=axes[1,0]
im3=ax.pcolormesh(unique_deltas, unique_cls,np.sqrt(varss_grid))
ax.set_xlabel('$\delta$')
ax.set_ylabel('$C_\lambda$')
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im3, cax=cax, orientation='vertical',label='$\lambda_{\mathrm{tip}}$ Std')


ax=axes[1,1]
runs_nan=runs
runs_nan[runs_nan<1]=np.nan
im4=ax.pcolormesh(unique_deltas, unique_cls,runs_nan)
ax.set_xlabel('$\delta$')
# ax.set_ylabel('$C_\lambda$')
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.05)
ax.set_title('Performed runs')
fig.colorbar(im4, cax=cax, orientation='vertical')
try:
    cma=co.tools.crop(co.cm.curl,vmax=np.max(skewss_grid[np.isnan(skewss_grid)==False]),vmin=np.min(skewss_grid[np.isnan(skewss_grid)==False]),pivot=0)
    ax=axes[1,2]
    im5=ax.pcolormesh(unique_deltas, unique_cls,skewss_grid,cmap=cma)
    ax.set_xlabel('$\delta$')
    # ax.set_ylabel('$C_\lambda$')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    ax.set_title('Scape time skewness \n (Bootstrapped)')
    fig.colorbar(im5, cax=cax, orientation='vertical',label='skewness')
except: print('skewness doesnt work')


lrelat=lscape_relative_to_delta(deltas,lscape)
means_l=np.zeros((len(cls),len(deltas)))

for d in range(len(deltas)):
    for j in range(len(cls)):
      means_l[j,d]=np.mean(lscape[j,d,np.isnan(lscape[j,d,:])==False])



cma2=co.tools.crop(co.cm.topo,vmax=np.max(means_l[np.isnan(means_l)==False]),vmin=25.0,pivot=25.01)
cmap1 = plt.cm.Blues
cmap2 = plt.cm.Oranges
piv=100*(25.01-np.min(means_l[np.isnan(means_l)==False]))/(np.max(means_l[np.isnan(means_l)==False])-np.min(means_l[np.isnan(means_l)==False]))
cma2=scol.stiched_colormap(cmap1, cmap2, piv)
ax=axes[0,2]
im6=ax.pcolormesh(unique_deltas, unique_cls,means_l,cmap=cma2)
# ax.set_xlabel('$\delta$')
# ax.set_ylabel('$C_\lambda$')
divider = make_axes_locatable(ax)
ax.set_title('mean $\lambda_{\mathrm{tip}}$')
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im6, cax=cax, orientation='vertical',label='mean $\lambda_{\mathrm{tip}}$')

plt.suptitle('Noise: %.2f, Ensemble size: %i' %(s,target_ensemble))
plt.tight_layout()
plt.show()     
try: fig.savefig(os.path.join(selected_folder,'first_result.png'), dpi=200) 
except: fig.savefig('./first_result.png', dpi=200)
#%%
folder_name = f"Analysis"
subfolder_name = f"lambda_histograms"
try:  os.makedirs(os.path.join(selected_folder,folder_name,subfolder_name))
except: print('folder exists')


pnorm=(cls-np.min(cls))/np.max(cls-np.min(cls))
np.sort(pnorm)
norm = BoundaryNorm(np.sort(cls), cmap.N)

lmax=25.01+deltas
l_t=25.01

for d in range(len(deltas)):
    fig=plt.figure(figsize=(14, 15))
    ax=fig.add_subplot(111)
    for j in range(len(cls)):
        tipped=lscape[j,d,:][lscape[j,d,:]>l0]
        if len(tipped)>0: 
            hist, bins = np.histogram(tipped, bins='auto', density=True)
            if np.diff(bins)[0]>0.5: hist, bins = np.histogram(tipped, bins=30, density=True)
            bin_centers = (bins[:-1] + bins[1:]) / 2
            # if np.diff(bins)[0]>0.5: ax.bar( bin_centers, 2*hist/np.max(hist), bottom=np.zeros_like(hist) + j*3, width=np.diff(bins)/10,fc=cmap(np.int64(255*pnorm[j])), ec=cmap(np.int64(255*pnorm[j])), align="center")
            # else: 
            #     ax.bar( bin_centers, 2*hist/np.max(hist), bottom=np.zeros_like(hist) + j*3, width=np.diff(bins),fc=cmap(np.int64(255*pnorm[j])), ec=cmap(np.int64(255*pnorm[j])), align="center")
            ax.bar( bin_centers, 2*hist/np.max(hist), bottom=np.zeros_like(hist) + j*3, width=np.diff(bins),fc=cmap(np.int64(255*pnorm[j])), ec=cmap(np.int64(255*pnorm[j])), align="center")
            ax.annotate('%.i of %.i:' %(len(tipped),runs[j,d])+' %.1f' %(100*len(tipped)/runs[j,d])+'%', 
                        (bin_centers[-1]+np.diff(bins)[0]/2+0.06, j*3+0.2), xytext=None,
                        xycoords='data', textcoords=None, fontsize=10, arrowprops=None, annotation_clip=None)
    ax.set_title('$\delta:$ %.3f \n Noise: %.2f, Ensemble size target: %i' %(deltas[d],s,target_ensemble))
    ax.set_xlabel('$\lambda$')
    ax.set_ylabel('$c_\lambda$')
    ax.set_ylim([0,(len(cls))*3])
    ax.set_xlim([l_t+2*deltas[0]-0.3,l_t+2*deltas[-1]+1.1])
    # Set the custom ticks on the x-axis
    ax.axvline(x=lmax[d], color='gray',linestyle='--',lw=1.5,label=r'$\lambda_{crit}+\delta$')
    ax.axvline(x=25.01, color='gray',linestyle='-',lw=1.5)
    # 25.01+deltas[d]-(float(value) - (25.01+deltas[d]))
    if deltas[d]>0:
        xticks = [l_t-0.5,l_t,l_t+deltas[d],l_t+2*deltas[d],l_t+2*deltas[d]+0.5,l_t+2*deltas[d]+1]
        ax.set_xticks(xticks)
        ax.set_xticklabels(['24.51','$\lambda_{cr}$','','$\lambda_{cr}$','24.51','24.01'])
        ax.axvspan(lmax[d]-(lmax[d]-l_t),lmax[d]+lmax[d]-l_t, alpha=0.25, color='gray',label='outside bifurcation')
    
    if deltas[d]<0:
        xticks = [l_t-0.5,l_t+deltas[d],l_t,]
        ax.set_xticks(xticks)
        ax.set_xticklabels(['24.51','','$\lambda_{cr}$'])
    
    plt.legend()
    
    # xticks = ax.get_xticks()
    # xticklabels = ax.get_xticklabels()
    # for label in xticklabels:
    #     x,  value = label.get_position(), label.get_text()
    #     if float(value) > (25.01+deltas[d]):
    #         new_value = '{:.2f}'.format(25.01+deltas[d]-(float(value) - (25.01+deltas[d])))
    #         label.set_text(new_value)
    # ax.set_xticklabels(xticklabels)

    yticks = ax.get_yticks()
    yticklabels = ax.get_yticklabels()
    for label in yticklabels:   
        y,  value = label.get_position(), label.get_text()
        new_value = '{:.3f}'.format(float(value)/(3*len(cls))*(cls[-1]-cls[0])+cls[0])
        label.set_text(new_value)
    ax.set_yticklabels(yticklabels)
    ax.set_yticklabels([])
    
    cbar_ax = fig.add_axes([0.91, 0.11, 0.025, 0.77])
    norm = BoundaryNorm(np.sort(cls), cmap.N)
    cbar=fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), cax=cbar_ax, ax=ax)
    cbar.set_label(r'$c_\lambda$', rotation=90,labelpad=3)
    formatter = ticker.ScalarFormatter(useMathText=True)
    formatter.set_scientific(True)
    formatter.set_powerlimits((-1, 1))
    cbar.ax.yaxis.set_major_formatter(formatter)

    try:  fig.savefig(os.path.join(selected_folder,folder_name,subfolder_name,str(d)+'.png'), dpi=200) 
    except:   fig.savefig('./gif_images_histograms/'+str(d)+'.png', dpi=200)

    # fig.savefig('./gif_images_histograms/'+str(d)+'.png', dpi=200)
    plt.close(fig)
    
    
    #%%
# folder_name = f"Analysis"
# subfolder_name = f"lambda_histograms_time"
# try:  os.makedirs(os.path.join(selected_folder,folder_name,subfolder_name))
# except: print('folder exists')


# pnorm=(cls-np.min(cls))/np.max(cls-np.min(cls))
# np.sort(pnorm)
# norm = BoundaryNorm(np.sort(cls), cmap.N)

# lmax=25.01+deltas
# l_t=25.01

# for d in range(len(deltas)):
#     fig=plt.figure(figsize=(14, 15))
#     ax=fig.add_subplot(111)
#     for j in range(len(cls)):
#         tipped=t_scape[j,d,:][lscape[j,d,:]>l0]/tfs[j,d]
#         if len(tipped)>0: 
#             hist, bins = np.histogram(tipped, bins='auto', density=True)
#             if np.diff(bins)[0]>0.5: hist, bins = np.histogram(tipped, bins=30, density=True)
#             bin_centers = (bins[:-1] + bins[1:]) / 2
#             # if np.diff(bins)[0]>0.5: ax.bar( bin_centers, 2*hist/np.max(hist), bottom=np.zeros_like(hist) + j*3, width=np.diff(bins)/10,fc=cmap(np.int64(255*pnorm[j])), ec=cmap(np.int64(255*pnorm[j])), align="center")
#             # else: 
#             #     ax.bar( bin_centers, 2*hist/np.max(hist), bottom=np.zeros_like(hist) + j*3, width=np.diff(bins),fc=cmap(np.int64(255*pnorm[j])), ec=cmap(np.int64(255*pnorm[j])), align="center")
#             ax.bar( bin_centers, 2*hist/np.max(hist), bottom=np.zeros_like(hist) + j*3, width=np.diff(bins),fc=cmap(np.int64(255*pnorm[j])), ec=cmap(np.int64(255*pnorm[j])), align="center")
#             ax.annotate('%.i of %.i:' %(len(tipped),runs[j,d])+' %.1f' %(100*len(tipped)/runs[j,d])+'%', 
#                         (bin_centers[-1]+np.diff(bins)[0]/2+0.06, j*3+0.2), xytext=None,
#                         xycoords='data', textcoords=None, fontsize=10, arrowprops=None, annotation_clip=None)
#     ax.set_title('$\delta:$ %.3f \n Noise: %.2f, Ensemble size target: %i' %(deltas[d],s,target_ensemble))
#     ax.set_xlabel('$time (normalized)$')
#     ax.set_ylabel('$c_\lambda$')
#     ax.set_ylim([0,(len(cls))*3])
#     ax.plot(0.5+0*cls-(deltas[d]/cls)/tfs[:,d],3*np.arange(0,len(cls),1),'-',color='grey',label='outside bifurcation')
#     ax.plot(0.5+deltas[d]/cls/tfs[:,d],3*np.arange(0,len(cls),1),'-',color='grey')
#     ax.plot(0.5+0*cls,3*np.arange(0,len(cls),1),'--k',label=r'$\lambda_{crit}+\delta$')
#     ax.set_xlim([0.48,0.54])
#     # Set the custom ticks on the x-axis
#     # ax.axvline(x=lmax[d], color='gray',linestyle='--',lw=1.5,label=r'$\lambda_{crit}+\delta$')
#     # ax.axvline(x=25.01, color='gray',linestyle='-',lw=1.5)
#     # 25.01+deltas[d]-(float(value) - (25.01+deltas[d]))
#     # if deltas[d]>0:
#     #     xticks = [l_t-0.5,l_t,l_t+deltas[d],l_t+2*deltas[d],l_t+2*deltas[d]+0.5,l_t+2*deltas[d]+1]
#     #     ax.set_xticks(xticks)
#     #     ax.set_xticklabels(['24.51','$\lambda_{cr}$','','$\lambda_{cr}$','24.51','24.01'])
#     #     ax.axvspan(lmax[d]-(lmax[d]-l_t),lmax[d]+lmax[d]-l_t, alpha=0.25, color='gray',label='outside bifurcation')
    
#     # if deltas[d]<0:
#     #     xticks = [l_t-0.5,l_t+deltas[d],l_t,]
#     #     ax.set_xticks(xticks)
#     #     ax.set_xticklabels(['24.51','','$\lambda_{cr}$'])
    
#     plt.legend()
    
#     # xticks = ax.get_xticks()
#     # xticklabels = ax.get_xticklabels()
#     # for label in xticklabels:
#     #     x,  value = label.get_position(), label.get_text()
#     #     if float(value) > (25.01+deltas[d]):
#     #         new_value = '{:.2f}'.format(25.01+deltas[d]-(float(value) - (25.01+deltas[d])))
#     #         label.set_text(new_value)
#     # ax.set_xticklabels(xticklabels)

#     yticks = ax.get_yticks()
#     yticklabels = ax.get_yticklabels()
#     for label in yticklabels:   
#         y,  value = label.get_position(), label.get_text()
#         new_value = '{:.3f}'.format(float(value)/(3*len(cls))*(cls[-1]-cls[0])+cls[0])
#         label.set_text(new_value)
#     ax.set_yticklabels(yticklabels)
#     ax.set_yticklabels([])
#     # plt.xscale('log')
#     cbar_ax = fig.add_axes([0.91, 0.11, 0.025, 0.77])
#     norm = BoundaryNorm(np.sort(cls), cmap.N)
#     cbar=fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), cax=cbar_ax, ax=ax)
#     cbar.set_label(r'$c_\lambda$', rotation=90,labelpad=3)
#     formatter = ticker.ScalarFormatter(useMathText=True)
#     formatter.set_scientific(True)
#     formatter.set_powerlimits((-1, 1))
#     cbar.ax.yaxis.set_major_formatter(formatter)

#     try:  fig.savefig(os.path.join(selected_folder,folder_name,subfolder_name,str(d)+'.png'), dpi=200) 
#     except:   fig.savefig('./gif_images_histograms_time/'+str(d)+'.png', dpi=200)

#     # fig.savefig('./gif_images_histograms/'+str(d)+'.png', dpi=200)
#     plt.close(fig)
    
        
#%%    
# import subprocess

# folder_path = os.path.join(selected_folder,folder_name,subfolder_name)

# # Call the script that creates the GIF animation and pass the folder path as an argument
# subprocess.run(["python", "gif_maker.py", folder_path])


# exec(open("aux_codes/gif_maker.py").read())    