# -*- coding: utf-8 -*-
"""
Created on Wed May 17 00:45:27 2023

@author: jp928814
"""

from PIL import Image
import glob
import re

# Create a list to store the image frames
frames = []

# Read the images and append them to the frames list
for image_file in sorted(glob.glob('*.png'), key=lambda x: int(re.findall(r'\d+', x)[0])):
    image = Image.open(image_file)
    frames.append(image)

# Save the frames as a GIF animation
frames[0].save('animation.gif', format='GIF',
               append_images=frames[1:],
               save_all=True,
               duration=300,  # Set the duration (in milliseconds) between frames
               loop=0)  # Set the loop value to 0 for an infinite loop, or any other positive integer for a finite loop
