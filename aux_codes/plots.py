# -*- coding: utf-8 -*-
"""
Created on Wed May 17 00:05:27 2023

Functions used for the saving bifurcation analysis

@author: jp928814
"""
import numpy as np
import sdeint
#import metrics
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import matplotlib.ticker as ticker
from matplotlib.colors import ListedColormap, BoundaryNorm, LinearSegmentedColormap
from matplotlib import cm
from matplotlib import colormaps as colm

plt.rcParams.update({'font.size': 18})
cmap =colm.get_cmap('turbo')

from population_func import *

def single_shot_plot(k,r,h,cls,delta,s):   
    pnorm=(cls-np.min(cls))/np.max(cls-np.min(cls))
    np.sort(pnorm)
    norm = BoundaryNorm(np.sort(cls), cmap.N)
    
    fig=plt.figure(figsize=(16,9))
    ax=fig.add_subplot(111)
    
    li=19
    l0=li
    lcrit=25.01+delta
    lf=lcrit    
    x0= 75
        
    for j in range(len(cls)):
      
        cl=np.sign(lf-l0)*cls[j]
        t_final=2*(lf-l0)/cl
        t_rm=np.abs(0.9)
        t_threshold=t_rm/2
        # dt=t_threshold/4
    
        datalen=t_final/300
        if datalen>=t_threshold/2: 
            datalen=t_threshold/2
    
        # t_win=t_rm/datalen ### length of correlation. 
        tspan=np.arange(0,t_final,datalen)
    
        def f(x, t):
            l=-cl*np.abs(t-t_final/2)+lcrit
            A=r*x*(1-x/k)-l*(x**2)/(x**2+h**2)
            return A
    
        def g(x, t):
            #B=[ 'noise in x'         0    ]
            #  [      0        'noise in r']
            B = s # diagonal, so independent driving Wiener processes
            #the only parameter with relevant aditive noise is X(t) in this case. 
            return B
        np.random.seed(1) #set numpy.random seed for reproducibility whenever it might be used.
        l1=-cl*np.abs(tspan-t_final/2)+lcrit
        # dl=cl*tspan[1]-l0
    
        #result = sdeint.itoint(f, g, x0, tspan)
        result = sdeint.stratHeun(f, g, x0, tspan)
        res=result
    
        ax.plot(l1,res,color=cmap(np.int64(255*pnorm[j])),alpha=0.8,lw=2)
    
    lvar=np.linspace(26,20,15000)
    ax.plot(lvar[np.isreal(s1(lvar))],s1(lvar)[np.isreal(s1(lvar))],'-k')
    ax.plot(lvar[np.isreal(s2(lvar))],s2(lvar)[np.isreal(s2(lvar))],'--k')
    ax.plot(lvar[np.isreal(s3(lvar))],s3(lvar)[np.isreal(s3(lvar))],'-k')
    ax.set_title('Additive noise: %.2f' %(s))
    
    
    cbar_ax = fig.add_axes([0.91, 0.11, 0.025, 0.77])
    norm = BoundaryNorm(np.sort(cls), cmap.N)
    cbar=fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), cax=cbar_ax, ax=ax)
    cbar.set_label(r'$c_\lambda$', rotation=90,labelpad=3)
    formatter = ticker.ScalarFormatter(useMathText=True)
    formatter.set_scientific(True)
    formatter.set_powerlimits((-1, 1))
    cbar.ax.yaxis.set_major_formatter(formatter)
    cbar.ax.yaxis.set_major_formatter(formatter)
    
    ax.annotate('$\delta=$ %.3f' %(delta), (lcrit+0.03,60), xytext=None, xycoords='data', textcoords=None, arrowprops=None, annotation_clip=None)
    ax.axvline(x=25.01, color='gray',lw=3,alpha=0.5)
    ax.axvline(x=lcrit,color='gray',lw=2, linestyle='--',alpha=0.2)
    ax.set_ylabel('Abundance')
    ax.set_xlabel('Harvesting rate ($\lambda$)')
    ax.set_ylim([-1,80])
    ax.set_xlim([22,lf+0.4])

    return fig