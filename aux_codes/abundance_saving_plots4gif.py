# -*- coding: utf-8 -*-
"""
Created on Wed May 17 00:26:10 2023

@author: jp928814
"""


import os 
cwd = os.getcwd()
import sys
#sys.path.append("C:/Users/928814/Documents/global_python_codes/Metrics_thresholds_boots") # go to parent dir
import sympy as sp
import numpy as np
#import metrics
import matplotlib.pyplot as plt


plt.rcParams.update({'font.size': 18})


from population_func import *
from plots import *

s=0.5
lvar=np.linspace(0,28,6900)
k=100
r=1
h=1 
cls = np.arange(0.0001,0.03,0.002)
deltas = np.arange(0.01,0.3,0.01)

n=0
for delta in deltas:   
    n+=1
    fig=single_shot_plot(k,r,h,cls,delta,s)
    fig.savefig('../gif_images/'+str(n)+'.png', dpi=200)
    plt.close(fig)