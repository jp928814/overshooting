# -*- coding: utf-8 -*-
"""
Created on Wed May 17 00:06:18 2023

Abundance functions
This is a simulation of a onlinear equation with multiplicative noise using a Heun method where the noise only applies on the variable and not the parameter. 

\begin{equation}
\dot{x}=[rx(1-x/K)-\lambda\frac{x^2}{x^2+h^2}]dt+\sigma x dW
\end{equation}

where $x$ is a random variable and $\lambda$ is a swiped parameter determined by $\dot{\lambda}=c_\lambda$. Here $c_\lambda=[0.025,0.03,...,0.07]$
r=1, k=100, h=1, c_0=1

@author: jp928814
"""
import numpy as np

def s1(lv):
    lv=lv+0*lv*1j
    return -(9997 - 300*lv)/(3*(45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)) - (45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)/3 + 100/3


def s2(lv):
    lv=lv+0*lv*1j
    return -(9997 - 300*lv)/(3*(-1/2 - np.emath.sqrt(3)*1j/2)*(45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)) - (-1/2 - np.emath.sqrt(3)*1j/2)*(45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)/3 + 100/3


def s3(lv):
    lv=lv+0*lv*1j
    return -(9997 - 300*lv)/(3*(-1/2 + np.emath.sqrt(3)*1j/2)*(45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)) - (-1/2 + np.emath.sqrt(3)*1j/2)*(45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)/3 + 100/3
