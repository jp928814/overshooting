# -*- coding: utf-8 -*-
"""
Created on Wed May 17 11:28:09 2023

reset runs

@author: jp928814
"""

import json
def reset_integrations():
    with open('temp_run_state.json', 'r+') as file: temp_vars = json.load(file)    # Load the JSON data
    if temp_vars['Running']==1:   
        temp_vars['Running']=0   
        with open('temp_run_state.json', 'r+') as file:
            json.dump(temp_vars, file)    # Your main code goes here
            print("reset done")

if __name__ == '__main__':
    reset_integrations()
    
    
   