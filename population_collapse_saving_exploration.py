# -*- coding: utf-8 -*-
"""
Created on Wed May 17 00:02:08 2023

@author: jp928814
"""

import os 
cwd = os.getcwd()
import sys
# import sympy as sp
import numpy as np
import sdeint
import json
import datetime
import pickle
import shutil
# from IPython.display import display, clear_output
from joblib import Parallel, delayed

sys.path.append("./aux_codes") # Directory of auxiliary modules
# from population_func import *
# from plots import *           

def integration_code(deltas,cls,s,k,r,h,max_ensemble,target_ensemble,runs,l0,x0,tfs,t_scape,t_rm,run_temp,n_runs,d_state,j_min,folder_name):
    threshold=30 #Threshold for the definition of tipped trayectory. 
    lcrit=25.01
    
    '''Main code body'''
    for d in range(d_state,len(deltas)):
        with open('temp_run_state.json', 'r+') as file: temp_vars = json.load(file)    # Load the JSON data
        with open('temp_run_state.json', 'r+') as file:
            temp_vars['d_state']=d
            temp_vars['run']=run_temp
            json.dump(temp_vars, file)    # Update run state
            # file.close()
        break_flag=False
    
        for j in range(len(cls)):
            lf = lcrit + deltas[d]
            cl = np.sign(lf-l0)*cls[j]
            t_final = 2*(lf-l0)/cl
            tfs[j,d] = t_final
                
            if j<j_min:
                t_scape[j,d,:]=t_scape[j,d-1,:]#copy from prevoius run
                runs[j,d]=runs[j,d-1] 
                np.save(os.path.join(folder_name,'t_scape.npy'),t_scape)
                np.save(os.path.join(folder_name,'tfs.npy'),tfs)    
                np.save(os.path.join(folder_name, 'runs.npy'), runs)
                
            else:
                t_threshold = t_rm/2
                # dt = t_threshold/2
                datalen = t_final/300
                if datalen>=t_threshold/2: 
                    datalen=t_threshold/2
                # t_win=t_rm/datalen ### length of correlation. 
                tspan=np.arange(0,t_final,datalen)
                
                def f(x, t):
                    l=-cl*np.abs(t-t_final/2)+lf
                    A=r*x*(1-x/k)-l*(x**2)/(x**2+h**2)
                    return A
                def g(x, t):
                    #B=[ 'noise in x'         0    ]
                    #  [      0        'noise in r']
                    B = s # diagonal, so independent driving Wiener processes
                    #the only parameter with relevant aditive noise is X(t) in this case. 
                    return B
        
                np.random.seed(1) #set numpy.random seed for reproducibility whenever it might be used.
           
                def process_data(i):
                    res = sdeint.stratHeun(f, g, x0, tspan)  # integration
                    try:
                        index = np.argwhere(res < threshold)[0][0]
                        return tspan[index]
                    except:
                        return 0 
                num_jobs = -1  # Use all available CPU cores, adjust as needed
    
                # t_scape_temp = Parallel(n_jobs=num_jobs)(delayed(process_data)(i) for i in range(target_ensemble))
                # t_scape[j,d,:] = np.array(t_scape_temp)
                # runs[j,d]+=target_ensemble
                print('\b\b\b\b\b\b\b'+'%.2f' %(((j+1)+len(cls)*d)/(len(cls)*len(deltas))*100)+'%',end='',flush=True)
    
                while((len(t_scape[j,d,t_scape[j,d,:]>0])<target_ensemble) and (runs[j,d]<max_ensemble)):
                    tipped_boolean=t_scape[j,d,:]>0
                    N_tipped=len(t_scape[j,d,tipped_boolean]) #runs to re-do. Only the ones that did not tip.
                    if break_flag==True: break
                    t_scape_temp = Parallel(n_jobs=num_jobs)(delayed(process_data)(i) for i in range(target_ensemble-N_tipped))
                    t_scape[j,d,tipped_boolean==False] = np.array(t_scape_temp)
                    
                    #If no trayectories tipped on the first run, stop running and go to next case.
                    tipped_boolean=t_scape[j,d,:]>0
                    if ((len(t_scape[j,d,tipped_boolean])<4) & (runs[j,d]<target_ensemble)): break_flag=True
                    runs[j,d]+=target_ensemble-N_tipped
                # state = {'d': d,'j':j}
                # with open('checkpoint.pkl', 'wb') as f:
                #     pickle.dump(state, f)
                run_temp+=runs[j,d]
                tipped_boolean=t_scape[j,d,:]>0

                #remember is all the trayectories tipped before lambda_cr+delta, if so, don't run it next time. 
                #it saves running stats that are already at equilibrium, and expensive to calculate.
                if (len(t_scape[j,d,tipped_boolean])==target_ensemble):
                    if (np.max(t_scape[j,d,tipped_boolean])/t_final<0.5):
                        j_min+=1
                        with open('temp_run_state.json', 'r+') as file:
                            temp_vars['j_min']=j_min
                # print('j',j,'d',d,'jmin',j_min) #debug print
                'update saved results after every complete set of stat runs'
                np.save(os.path.join(folder_name,'t_scape.npy'),t_scape)
                np.save(os.path.join(folder_name,'tfs.npy'),tfs)    
                np.save(os.path.join(folder_name, 'runs.npy'), runs)
                
    reset_integrations()
      




# def resume_from_checkpoint():
#     try:
#         with open('checkpoint.pkl', 'rb') as f:
#             state = pickle.load(f)
#         d,j= state['d'],state['j'] 
#         integration_code(deltas,cls,s,k,r,h,cases,l0,x0,tfs,t_scape,t_rm,run_temp,n_runs,d_state,folder_name)
#     except FileNotFoundError:
#         print("No checkpoint found. Starting from the beginning.")
#         integration_code(deltas,cls,s,k,r,h,cases,l0,x0,tfs,t_scape,t_rm,run_temp,n_runs,d_state,folder_name)        
        
def reset_integrations():
    with open('temp_run_state.json', 'r+') as file: temp_vars = json.load(file)    # Load the JSON data
    if temp_vars['Running']==1:   
        temp_vars['Running']=0   
        with open('temp_run_state.json', 'r+') as file:
            json.dump(temp_vars, file)    # Your main code goes here
            print("reset done")


def find_first_nonzero_index(arr):
    nonzero_indices = np.nonzero(arr)[0]
    if len(nonzero_indices) > 0:
        return nonzero_indices[0]
    return -1  # If no non-zero element is found


###### Main code ######
with open('temp_run_state.json', 'r') as file: temp_vars = json.load(file)    # Load the JSON data

if temp_vars['Running']==1:  #'''Check if it was running from before'''
    print('Resuming previous run')
    folder_name=temp_vars['folder']
    s=temp_vars['Noise']
    
    k,r,h=temp_vars['Parameters']
    l0,x0=temp_vars['Initial conditions']
    # cases=temp_vars['Cases']
    run=temp_vars['run']
    t_rm=temp_vars['t_rm']
    d_state=temp_vars['d_state']
    j_min=temp_vars['j_min']
    max_ensemble,target_ensemble=temp_vars['ensemble_vars']

    cls=np.load(os.path.join(folder_name,'cls.npy'))
    deltas=np.load(os.path.join(folder_name,'deltas.npy'))
    t_scape=np.load(os.path.join(folder_name,'t_scape.npy'))
    tfs=np.load(os.path.join(folder_name,'tfs.npy'))
    runs=np.load(os.path.join(folder_name,'runs.npy'))

    index = find_first_nonzero_index(runs.flatten(order='F')[::-1])

    n_runs=len(cls)*len(deltas)*target_ensemble
    
    print('Min number of runs',n_runs,'\n','Previous Progress: %.2f '%((index//len(cls)+1)/n_runs*100)+'%')
    run_temp=run
    
    integration_code(deltas,cls,s,k,r,h,max_ensemble,target_ensemble,runs,l0,x0,tfs,t_scape,t_rm,run_temp,n_runs,d_state,j_min,folder_name)

    # resume_from_checkpoint()

else: #'''Set up new run'''
    # Create a timestamp for the folder name
    timestamp = datetime.datetime.now().strftime("%d_%m_%Y_%H%M%S")
    folder_name = f"output_{timestamp}"
    os.makedirs(folder_name)
    # Save files inside the created folder
    # file1_path = os.path.join(folder_name, "file1.txt")    

    '''Set code variables'''
    s=0.5
    k=100
    r=1
    h=1 
    
    #map experiment grid (if i dont fix the time outside the bifurcation)
    cls = np.arange(0.0002,0.002,0.0001)
    deltas = np.arange(-0.1,0.1,0.005)
    np.save(os.path.join(folder_name, 'cls.npy')  , cls)
    np.save(os.path.join(folder_name, 'deltas.npy'), deltas)
    
    #single_shot_plot(k,r,h,cls,deltas[0],s)
    target_ensemble=300 #tipped stats target population
    max_ensemble=2*target_ensemble #max number of runs 

    missed_target_counter=0 #times the code didn't managed to get to the target ensemble. 
    #if missed_target_counter>4, stop running, all trayectories are considered safe.
    
    #add: if none trayec. is saved in the initial run, stop the loop for the subsecuent runs at that delta.
    runs=np.zeros((len(cls),len(deltas))) #saves amount of runs for each case.
    np.save(os.path.join(folder_name, 'runs.npy')  , runs)

    cases=target_ensemble
    
    
    # threshold=30 #Threshold for the definition of tipped trayectory. 
    t_scape=np.zeros((len(cls),len(deltas),target_ensemble))
    tfs=np.zeros((len(cls),len(deltas)))
    np.save(os.path.join(folder_name,'t_scape.npy'), t_scape)
    np.save(os.path.join(folder_name,'tfs.npy'), tfs)

    
    n_runs=len(cls)*len(deltas)*target_ensemble
    print('Total number of runs',n_runs,'\n')
    run=0 #run counter to keep track of code progress
    vars_tuple=[]
    
    #set integration
    l0 = 20
    x0 = 75
    t_rm=np.abs(1.2)
    j_min=0 #tracks stats that can be skipped
    d_state=0 #keep track of integration
    ################ Set run temp file to running, so it can be stopped and resumed
    Run_state = 1  # Code Running state. 1 If it hasn't finished running the previous run
    temp_vars = {'Running': Run_state,'Noise': s,'Parameters':[k,r,h],
                 'Initial conditions':[l0,x0],'t_rm':t_rm,
                 'run':run,'folder':folder_name,'d_state':d_state,'j_min':j_min,'ensemble_vars':[max_ensemble,target_ensemble]}
    with open('temp_run_state.json', 'w') as file: json.dump(temp_vars, file) #update json temp values.
 
    run_temp=run
    shutil.copy('temp_run_state.json', os.path.join(folder_name,'temp_run_state.json') )     
    print('Progress:')  
    
    integration_code(deltas,cls,s,k,r,h,max_ensemble,target_ensemble,runs,l0,x0,tfs,t_scape,t_rm,run_temp,n_runs,d_state,j_min,folder_name)

                
   

