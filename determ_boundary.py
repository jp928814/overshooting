# -*- coding: utf-8 -*-
"""
Created on Mon Jun  5 13:43:58 2023

@author: jp928814
"""

import os 
cwd = os.getcwd()
import sys
sys.path.append("./aux_codes") # Directory of auxiliary modules

# import sympy as sp
import numpy as np
import sdeint
import json
import datetime
import pickle
import shutil
# from IPython.display import display, clear_output
from joblib import Parallel, delayed
from scipy.integrate import odeint
import matplotlib.ticker as ticker
from matplotlib.colors import ListedColormap, BoundaryNorm, LinearSegmentedColormap
from matplotlib import cm
cmap =cm.get_cmap('turbo')
import matplotlib.pyplot as plt

k=100
r=1
h=1 

def s1(lv):
    lv=lv+0*lv*1j
    return -(9997 - 300*lv)/(3*(45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)) - (45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)/3 + 100/3


def s2(lv):
    lv=lv+0*lv*1j
    return -(9997 - 300*lv)/(3*(-1/2 - np.emath.sqrt(3)*1j/2)*(45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)) - (-1/2 - np.emath.sqrt(3)*1j/2)*(45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)/3 + 100/3


def s3(lv):
    lv=lv+0*lv*1j
    return -(9997 - 300*lv)/(3*(-1/2 + np.emath.sqrt(3)*1j/2)*(45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)) - (-1/2 + np.emath.sqrt(3)*1j/2)*(45000*lv + np.emath.sqrt(-4*(9997 - 300*lv)**3 + (90000*lv - 2001800)**2)/2 - 1000900)**(1/3)/3 + 100/3

lvar=np.linspace(0,28,6900)
j_min=0 #tracks stats that can be skipped

cls = np.arange(0.00001,0.01,0.00001)
deltas = np.arange(0.001,0.1,0.001)
t_scape_det=np.zeros((len(cls),len(deltas)))
tfs_det=np.zeros((len(cls),len(deltas)))

threshold=30 #Threshold for the definition of tipped trayectory. 
print('Progress:\n')

for d in range(len(deltas)):
    break_flag=False

    for j in range(len(cls)):
        x0= 75
        li=20
        l0=li
        lcrit=25.01+deltas[d]
        lf=lcrit
        cl = np.sign(lf-l0)*cls[j]
    
        t_final=2*(lf-l0)/cl
        tfs_det[j,d]=t_final

        if j<j_min:
            t_scape_det[j,d]=t_scape_det[j,d-1]#copy from prevoius run
            print('\b\b\b\b\b\b\b'+'%.2f' %(((j+1)+len(cls)*d)/(len(cls)*len(deltas))*100)+'%',end='',flush=True)

            # print('j','d')
        else:
            t_rm=np.abs(0.9)
            t_threshold = t_rm/2
            # dt = t_threshold/2
            datalen = t_final/300
            if datalen>=t_threshold/2: 
                datalen=t_threshold/2
        
            # t_win=t_rm/datalen ### length of correlation. 
            tspan=np.arange(0,t_final,datalen)
        
            def f(x, t):
                l=-cl*np.abs(t-t_final/2)+lf
                A=r*x*(1-x/k)-l*(x**2)/(x**2+h**2)
                return A
    
            res_det=odeint(f, x0, tspan)
            try:
                index = np.argwhere(res_det < threshold)[0][0]
                if t_scape_det[j,d]/t_final<0.49: j_min+=1
            except:
                index = 0 
                break_flag=True
            t_scape_det[j,d]=tspan[index]
            tfs_det[j,d]=t_final
            
                        
            print('\b\b\b\b\b\b\b'+'%.2f' %(((j+1)+len(cls)*d)/(len(cls)*len(deltas))*100)+'%',end='',flush=True)
            if break_flag==True: break
print('integration finished')

#%%       
means_det=np.zeros((len(cls),len(deltas)))
counts_det=np.zeros((len(cls),len(deltas)))    
vars_tuple=[]

for d in range(len(deltas)):
    for j in range(len(cls)):
        vars_tuple.append((cls[j],deltas[d]))
        scaped=t_scape_det[j,d]
        means_det[j,d]=scaped / (tfs_det[j,d]-2*deltas[d]/cls[j])
        if scaped>0:  
            counts_det[j,d]=1
        else: 
            counts_det[j,d]=0
means_det[means_det<0.1]=np.nan
results=[]
import csv
import pandas as pd

for j in range(len(counts_det.flatten())):
    results.append((vars_tuple[j][0],vars_tuple[j][1],counts_det.flatten()[j],means_det.flatten()[j])) 
        
with open('stats2_det.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Cl', 'delta', 'count','mean'])  # Write the header row
    writer.writerows(results)  

# Read the CSV file into a pandas DataFrame
df = pd.read_csv('stats2_det.csv')

# Extract variable values and results from the DataFrame

results = df['count'].values
means = df['mean'].values

cls_flat = df['Cl'].values
deltas_flat = df['delta'].values
# Get the unique values and their corresponding indices
unique_cls, cls_indices = np.unique(cls_flat, return_inverse=True)
unique_deltas, delta_indices = np.unique(deltas_flat, return_inverse=True)

# Reshape results array to match the shape of unique_cls and unique_deltas
results_grid = results.reshape(len(unique_cls), len(unique_deltas))
means_grid = means.reshape(len(unique_cls), len(unique_deltas))

runs=1
probs_grid=100*results_grid/runs
probs_grid[np.isnan(probs_grid)]=0
#%%
from mpl_toolkits.axes_grid1 import make_axes_locatable
import cmocean as co

import stiched_colormap as scol
shad=None
# Plotting the pcolor
fsz=(18, 12)
fig, axes = plt.subplots(2, 2, sharey=True,figsize=fsz)
ax=axes[0,0]

cmap1 = plt.cm.RdYlBu_r
cmap2 = plt.cm.RdYlGn
cma3=scol.stiched_colormap(cmap1, cmap2, 50)
im1=ax.pcolormesh(unique_deltas, unique_cls, probs_grid,cmap=cma3,shading=shad)############################

idex_max=np.argwhere(probs_grid[-1,:] > 0)[0][0]
deltas_bound=np.copy(deltas[0:idex_max])
boundary=np.zeros((idex_max))
cls_temp=np.linspace(np.min(cls),np.max(cls),len(deltas))
for d in range(len(deltas_bound)):
    boundary[d]=cls[np.argwhere(probs_grid[:,d] < 100)[0][0]]


ax.plot(deltas_bound,boundary,'--r')
ax.set_xlabel('$\delta$')
ax.set_ylabel('$C_\lambda$')
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im1, cax=cax, orientation='vertical',label='Tipped percentage')

ax=axes[0,1]
im2=ax.pcolor(unique_deltas, unique_cls,  means_grid,shading=shad)
ax.set_xlabel('$\delta$')
ax.set_ylabel('$C_\lambda$')
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im2, cax=cax, orientation='vertical',label='Mean scape time')



def tscape_to_lscape(t_scape,tfs,deltas,l0): 
    lmax=25.01+deltas
    tscp_norm=np.zeros_like(t_scape)
    lscape=np.zeros_like(t_scape)
    tscp_norm[:,:]=t_scape[:,:]/tfs
    # print(tscp_norm)
    for d in range(len(deltas)):
        lmax=25.01+deltas[d]
        lscape[:,d]=tscp_norm[:,d]*(2*(lmax-l0))+l0
    return lscape

lscape=tscape_to_lscape(t_scape_det,tfs_det,deltas,l0)
lscape[lscape<24]=np.nan


def tscape_to_lscape(t_scape,tfs,deltas,l0): 
    lmax=25.01+deltas
    tscp_norm=np.zeros_like(t_scape)
    lscape=np.zeros_like(t_scape)
    tscp_norm[:,:]=t_scape[:,:]/tfs
    # print(tscp_norm)
    for d in range(len(deltas)):
        lmax=25.01+deltas[d]
        lscape[:,d]=tscp_norm[:,d]*(2*(lmax-l0))+l0
    return lscape

lscape=tscape_to_lscape(t_scape_det,tfs_det,deltas,l0)
lscape[lscape<24]=np.nan
def lscape_relative_to_delta(deltas,lscape):
    lrelat=np.zeros_like(lscape)
    for d in range(len(deltas)):
        lrelat[:,d]=lscape[:,d]-(25.01+deltas[d])
    return lrelat

lrelat=lscape_relative_to_delta(deltas,lscape)


ax=axes[1,0]
# cma=co.tools.crop(co.cm.curl,vmax=np.max(lscape[np.isnan(lrelat)==False]),vmin=np.min(lscape[np.isnan(lrelat)==False]),pivot=0)
cma=co.tools.crop(co.cm.curl,vmax=np.max(lrelat[np.isnan(lrelat)==False]),vmin=np.min(lrelat[np.isnan(lrelat)==False]),pivot=0)

im3=ax.pcolormesh(unique_deltas, unique_cls,  lrelat,shading=shad,cmap=cma)
ax.set_xlabel('$\delta$')
ax.set_ylabel('$C_\lambda$')
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.05)
ax.set_title('relative $\lambda$ scape')
fig.colorbar(im3, cax=cax, orientation='vertical',label='relative $\lambda$ scape')


np.save('deltas_det.npy',deltas)
np.save('cls_det.npy',cls)
np.save('counts_det.npy',counts_det)
np.save('scape_det.npy',means_det)


ax=axes[1,1]
im4=ax.pcolormesh(unique_deltas, unique_cls,  lscape,shading=shad)
ax.set_xlabel('$\delta$')
ax.set_ylabel('$C_\lambda$')
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im4, cax=cax, orientation='vertical',label='scape $\lambda$')


np.save('deltas_det.npy',deltas)
np.save('cls_det.npy',cls)
np.save('counts_det.npy',counts_det)
np.save('scape_det.npy',means_det)
np.save('deltas_bound.npy',deltas_bound)
np.save('cls_bound.npy',boundary)
# varss_grid = runs.reshape(len(unique_cls), len(unique_deltas))
fig.savefig('Scape_boundary_deterministic.png', dpi=200)

# lvar=np.linspace(26,21,19000)
# ax.plot([25.01,25.01],[0,100],color='gray',lw=3,alpha=0.5)
# ax.plot(lvar[np.isreal(s1(lvar))],s1(lvar)[np.isreal(s1(lvar))],'-k')
# ax.plot(lvar[np.isreal(s2(lvar))],s2(lvar)[np.isreal(s2(lvar))],'--k')
# ax.plot(lvar[np.isreal(s3(lvar))],s3(lvar)[np.isreal(s3(lvar))],'-k')
# ax.set_title('Deterministic')


# cbar_ax = fig.add_axes([0.91, 0.11, 0.025, 0.77])
# norm = BoundaryNorm(np.sort(cls), cmap.N)
# cbar=fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), cax=cbar_ax, ax=ax)
# cbar.set_label(r'$c_\lambda$', rotation=90,labelpad=3)
# formatter = ticker.ScalarFormatter(useMathText=True)
# formatter.set_scientific(True)
# formatter.set_powerlimits((-1, 1))
# cbar.ax.yaxis.set_major_formatter(formatter)

# ax.set_ylabel('Abundance')
# ax.set_xlabel('Harvesting rate ($\lambda$)')
# ax.set_ylim([-1,80])
# ax.set_xlim([22,lf+0.5])
# fig.savefig('Deterministic_linearperiodic_protocol.png', dpi=200)

#%%

fsz=(8, 4.5)
fig=plt.figure(figsize=fsz)
ax=plt.subplot(111)

cmap1 = plt.cm.RdYlBu_r
cmap2 = plt.cm.RdYlGn
cma3=scol.stiched_colormap(cmap1, cmap2, 50)
im1=ax.pcolormesh(unique_deltas, unique_cls, probs_grid,cmap=cma3,shading=shad)############################

idex_max=np.argwhere(probs_grid[-1,:] > 0)[0][0]
deltas_bound=np.copy(deltas[0:idex_max])
boundary=np.zeros((idex_max))
cls_temp=np.linspace(np.min(cls),np.max(cls),len(deltas))
for d in range(len(deltas_bound)):
    boundary[d]=cls[np.argwhere(probs_grid[:,d] < 100)[0][0]]


ax.plot(deltas_bound,boundary,'--r')
ax.set_xlabel('$\delta$')
ax.set_ylabel('$C_\lambda$')
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.05)
plt.tight_layout()
fig.colorbar(im1, cax=cax, orientation='vertical',label='Tipped percentage')
