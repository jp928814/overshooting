\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Inspiration}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Example}{1}{section.1.2}%
\contentsline {section}{\numberline {1.3}Sanity checks: Ghosting decay and others}{1}{section.1.3}%
\contentsline {chapter}{\numberline {2}Overshoot saving. Linear protocol, additive noise}{4}{chapter.2}%
\contentsline {section}{\numberline {2.1}High noise stats}{4}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Very high noise}{6}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Ideas and things to do:}{6}{subsection.2.1.2}%
\contentsline {chapter}{\numberline {3}Saving map analysis}{13}{chapter.3}%
\contentsline {section}{\numberline {3.1}Saving statistics for 'medium noise intensity'}{13}{section.3.1}%
\contentsline {chapter}{\numberline {4}Overshooting problem as a period-1 problem}{17}{chapter.4}%
\contentsline {subsection}{\numberline {4.0.1}Possible problems}{17}{subsection.4.0.1}%
\contentsline {chapter}{\numberline {5}FP-integrations}{21}{chapter.5}%
\contentsline {section}{\numberline {5.1}1-D Example}{21}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Slow and low noise:}{21}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Slow and high noise:}{21}{subsection.5.1.2}%
\contentsline {subsection}{\numberline {5.1.3}Medium speed saved and low noise:}{24}{subsection.5.1.3}%
\contentsline {subsection}{\numberline {5.1.4}Medium speed saved and high noise:}{24}{subsection.5.1.4}%
\contentsline {chapter}{\numberline {6}Appendix}{27}{chapter.6}%
\contentsline {section}{\numberline {6.1}Numerical schemes}{27}{section.6.1}%
\contentsline {section}{\numberline {6.2}1D Cranck-Nicholson finite difference scheme}{27}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Alternative expressions}{28}{subsection.6.2.1}%
\contentsline {subsubsection}{keeping $\partial _x (F\rho )$ }{28}{section*.29}%
\contentsline {subsubsection}{explicit $\partial _x F $}{29}{section*.30}%
\contentsline {subsection}{\numberline {6.2.2}FP integrations literature}{29}{subsection.6.2.2}%
\contentsline {subsection}{\numberline {6.2.3}codes}{29}{subsection.6.2.3}%
